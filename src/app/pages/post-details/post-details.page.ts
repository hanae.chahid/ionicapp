import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.page.html',
  styleUrls: ['./post-details.page.scss'],
})
export class PostDetailsPage implements OnInit {
  post: any;

  constructor(public postService: PostService, public route: Router) { }

  ngOnInit() {
    this.post = this.postService.currentPost;
    console.log(this.post);
  }

}
