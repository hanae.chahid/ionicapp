import { Component, OnInit } from '@angular/core';
import { PostService} from '../services/post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  posts: any = [];
  image: any;
  page = 0;
  constructor(public postService: PostService, public router: Router ) {
   }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.postService.getAllPosts(this.page)
    .subscribe(response => {
      this.posts = response;
    }, error => {
      console.log(error);
    });
  }

  loadMorePosts(event) {
    console.log(event);
    this.page = this.page + 10;
    this.postService.getAllPosts(this.page)
    .subscribe(response => {
      for ( const post of response) {
        this.posts.push(post);
      }
      event.target.complete();
    }, error => {
      console.log(error);
    });
  }

  goToDetailsPage(post) {
    this.postService.currentPost = post ;
    this.router.navigate(['post-details']);
  }

}
