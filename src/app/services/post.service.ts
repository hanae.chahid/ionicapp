import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  currentPost: any;
  constructor(private http: HttpClient) { }

  getAllPosts(page) {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts?_start=${page}&_limit=10`);

  }
}
